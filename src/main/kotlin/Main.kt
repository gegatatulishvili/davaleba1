import kotlin.math.sqrt

class Fraction(var top: Int, var bottom: Int) {

    fun simplify() {
        var gcd = 1
        val min = minOf(top, bottom)
        for (i in 1..(min + 1)){
            if (top % i == 0 && bottom % i == 0){
                gcd = i
            }
        }

        top /= gcd
        bottom /= gcd
    }

    fun add(fraction: Fraction): Fraction{
        val temp = Fraction(
            this.top * fraction.bottom + fraction.top * this.bottom,
            this.bottom * fraction.bottom)
        temp.simplify()
        return temp
    }
    fun subtract(fraction: Fraction): Fraction{
        val temp = Fraction(
            this.top * fraction.bottom - fraction.top * this.bottom,
            this.bottom * fraction.bottom)
        temp.simplify()
        return temp
    }
    fun multiple(fraction: Fraction): Fraction{
        val temp = Fraction(
            this.top * fraction.top,
            this.bottom * fraction.bottom)
        temp.simplify()
        return temp
    }
    fun divide(fraction: Fraction): Fraction{
        val temp = Fraction(
            this.top * fraction.bottom,
            this.bottom * fraction.top)
        temp.simplify()
        return temp
    }

    override fun toString(): String {
        return "$top / $bottom"
    }

    override fun equals(other: Any?): Boolean {
        if (other is Fraction) {
            return (this.top == other.top && this.bottom == other.bottom)
        }
        return false
    }
}


class Point(var x: Int, var y: Int) {

    fun move_symmetrically() {
        x = -x
        y = -y
    }

    fun distance(point: Point): Float{
        val dx = this.x - point.x
        val dy = this.y - point.y
        val d = ((dx * dx) + (dy * dy)).toFloat()
        return sqrt(d)
    }


    override fun toString(): String {
        return "($x, $y)"
    }

    override fun equals(other: Any?): Boolean {
        if (other is Point){
            return this.x == other.x && this.y == other.y
        }
        return false
    }
}

fun main(args: Array<String>) {
    val p1 = Point(10, 12)
    val p2 = Point(14, 13)
    val p3 = Point(10, 12)

    println(p1)
    p1.move_symmetrically()
    println(p1)
    p1.move_symmetrically()
    println(p1)

    println(p1 == p3)
    println(p1 == p2)

    println(p1.distance(p2))

//    ლექციაზე დაწერილი კოდი ვერ ვნახე ვერსად, ამიტომ თავიდან დავწერე ჩემით

    val f1 = Fraction(12, 16)
    val f2 = Fraction(1, 3)
    val f3 = Fraction(10, 15)

    println(f1)
    f1.simplify()
    println(f1)
    f1.simplify()
    println(f1)
    println(f3)
    f3.simplify()
    println(f3)

    println(f1.add(f2))
    println(f2.add(f1))
    println(f1.add(f3))

    println(f1.subtract(f2))
    println(f2.subtract(f1))
    println(f1.subtract(f3))

    println(f1.multiple(f2))
    println(f2.multiple(f1))
    println(f1.multiple(f3))

    println(f1.divide(f2))
    println(f2.divide(f1))
    println(f1.divide(f3))
}